# exe2bin

Convert an exe file to bin format

# Contributing

**Would you like to contribute to FreeDOS?** The programs listed here are a great place to start. Most of these do not have a maintainer anymore and need your help to make them better. Here's how to get started:

* __Maintainers__: Let us know if you'd like to take on one of these programs, and we can provide access to the source code repository here. Please use the FreeDOS package structure when you make new releases, including all executables, source code, and metadata.

* __New developers__: We can extend access for you to track issues here.

* __Translators__: Please submit to the [FD-NLS Project](https://github.com/shidel/fd-nls).

_*Make sure to check all source code licenses, especially for any code you might reuse from other projects to improve these programs. Note that not all open source licenses are the same or compatible with one another. (For example, you cannot reuse code covered under the GNU GPL in a program that uses the BSD license.)_

## EXE2BIN.LSM

<table>
<tr><td>title</td><td>exe2bin</td></tr>
<tr><td>version</td><td>1.5a</td></tr>
<tr><td>entered&nbsp;date</td><td>2006-07-24</td></tr>
<tr><td>description</td><td>Convert an exe file to bin format</td></tr>
<tr><td>keywords</td><td>freedos</td></tr>
<tr><td>author</td><td>ramax@maks.bhg.ru</td></tr>
<tr><td>maintained&nbsp;by</td><td>ramax@maks.bhg.ru</td></tr>
<tr><td>platforms</td><td>dos</td></tr>
<tr><td>copying&nbsp;policy</td><td>[Sybase Open Watcom Public License, Version 1.0](LICENSE)</td></tr>
<tr><td>wiki&nbsp;site</td><td>http://wiki.freedos.org/wiki/index.php/Exe2bin</td></tr>
</table>
